package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.Person;
import util.JpaUtil;

public class PersonDao {
	
	public List<Person> findAllPersons() {
		EntityManager em = null;
		try {
			em = JpaUtil.getFactory().createEntityManager();

			TypedQuery<Person> query = em.createQuery("select p from Person p", Person.class);

			return query.getResultList();

		} finally {
			JpaUtil.closeQuietly(em);
		}
	}
	
	public Person findPersonByName(String name) {
		EntityManager em = null;
		try {
			em = JpaUtil.getFactory().createEntityManager();

			TypedQuery<Person> query = em.createQuery("select p from Person p where p.name = :name", Person.class);

			query.setParameter("name", name);

			return query.getSingleResult();

		} finally {
			JpaUtil.closeQuietly(em);
		}
	}
	
	public void savePerson(Person person) {
		EntityManager em = null;
		try {
			em = JpaUtil.getFactory().createEntityManager();

			em.getTransaction().begin();

			if (person.getId() == null) {
				// persisti kasutame ainult uue kirje lisamiseks
				em.persist(person);
			} else {
				// merge't kirje uuendamiseks
				em.merge(person);
			}

			em.getTransaction().commit();
		} finally {
			JpaUtil.closeQuietly(em);
		}
	}

	public void insertPerson(String name) {
		savePerson(new Person(name));
	}
	
}
