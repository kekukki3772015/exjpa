package main;

import java.util.Arrays;

import dao.PersonDao;
import dao.SetupDao;
import model.Address;
import model.Person;
import model.Phone;
import util.JpaUtil;

public class Main {

	//private PersonDao dao = new PersonDao();
	
	public static void main(String[] args) {

		new SetupDao().createSchema();
		PersonDao dao = new PersonDao();
		
		dao.insertPerson("Jack");
		dao.insertPerson("Jill");
		dao.insertPerson("John");

		System.out.println(dao.findAllPersons());
		
		System.out.println(dao.findPersonByName("Jill"));

		Person jill = dao.findPersonByName("Jill");
		
		jill.setName("Jillo");
		dao.savePerson(jill);
		
		jill.setAddress(new Address("Tamme 1"));
		dao.savePerson(jill);
		
		jill = dao.findPersonByName("Jillo");
		System.out.println(jill.getAddress());
		
		jill.setPhones(Arrays.asList(new Phone("123"),new Phone("456")));
		dao.savePerson(jill);
		System.out.println(jill.getPhones());
		
		System.out.println(dao.findAllPersons());

		JpaUtil.closeFactory();
	}

}
