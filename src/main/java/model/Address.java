package model;

import javax.persistence.Entity;

@Entity
public class Address  extends BaseEntity{

	private String street;

	public Address() {
	}

	public Address(String street) {
		this.street = street;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Override
	public String toString() {
		return street;
	}

}
