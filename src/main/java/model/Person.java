package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Person extends BaseEntity {

	private String name;
	
	//cascade on (aadressi) salvestamiseks, sest vaikimisiseda ei tehta
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	
	//FetchType.EAGER tähendab seda, et toimuks laiska väärtustamist. By default on FetchType.LAZY
	//Piltlikult Person on One poole peal ja Phone on Many poolel.
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "person_id", nullable = false)
	private List<Phone> phones;

	public Person() {
	}

	public Person(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Phone> getPhones() {
		return phones;
	}

	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
}
